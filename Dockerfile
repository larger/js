FROM alpine:latest

RUN set -ex \
	&& apk add --no-cache libsodium py2-pip \
    && pip --no-cache-dir install https://github.com/shadowsocks/shadowsocks/archive/master.zip
	
ENTRYPOINT ["/usr/bin/ssserver"]